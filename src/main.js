import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './utils/dayjs.js'
// vant 样式库
import Vant from 'vant'
import 'vant/lib/index.css'

// 引入全局样式
import './style/index.less'

// 配置项目rem基准值
// 1. npm i amfe-flexible 下载 lib-flexible 依赖
// 2. 在main.js中引入相应依赖
// 3. 当使用后，html标签就会设置一个当前设备视口像素宽度的10%来作为font-size属性
// 4. 通过postcss-pxtorem 插件可以将px转化为rem，但是在实际的操作中，该插件不可以修改行内样式的px
// 5. 如果移动端项目中的样式涉及到自适应时，不可以写在行内样式中
import 'amfe-flexible'
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
