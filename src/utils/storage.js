// 封装本地存储模块

// 增加或修改数据
export const setItem = (key, value) => {
  // 判断value是否为OBJECT类型，如果是，那么进行json字符串的转化
  if (typeof value === 'object') {
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(key, value)
}

// 获取数据
export const getItem = key => {
  const data = window.localStorage.getItem(key)
  try {
    return JSON.parse(data)
  } catch (error) {
    return data
  }
}

// 删除数据
export const removeitem = key => {
  window.localStorage.removeItem(key)
}
