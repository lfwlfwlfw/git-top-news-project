import axios from 'axios'
import store from '../store/index.js'
export const request = axios.create({
  baseURL: 'http://toutiao.itheima.net/'
})

// axios 请求拦截器
request.interceptors.request.use(config => {
  if (store.state.user.token) {
    config.headers.Authorization = `Bearer ${store.state.user.token}`
  }
  return config
})
