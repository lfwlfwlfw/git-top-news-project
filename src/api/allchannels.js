import { request } from '../utils/request.js'

export const getAllChannels = () => request({
  /*
   获取全部频道列表
    */
  method: 'GET',
  url: '/v1_0/channels'
})

export const addUserChannels = (item) => request({
  /*
  获取新增频道列表
  @params item.seq 频道顺序  list.length

  */
  method: 'PATCH',
  url: '/v1_0/user/channels',
  data: {
    channels: [item]
  }
})
export const deleteUserChannels = (channelId) => request({
  /*
  获取新增频道列表
  @params channelId

  */
  method: 'DELETE',
  url: `/v1_0/user/channels/${channelId}`
})
