// 搜索相关接口模块
import { request } from '../utils/request.js'

export const getSuggestions = (params) => request({
  /*
    获取新增频道列表
    @params q 联想词缀
    */
  method: 'GET',
  url: '/v1_0/suggestion',
  params
})
export const getResults = (params) => request({
  /*
    获取新增频道列表
    @params q 联想词缀
    @params page 页数，不传默认为1
    @params per_page 每页数量，不传每页数量由后端决定
    */
  method: 'GET',
  url: '/v1_0/search',
  params
})
export const getSearchHistories = (params) => request({
  /*
    获取用户搜索历史
    @params message 联想词缀
    */
  method: 'GET',
  url: '/v1_0/search/histories',
  params
})
