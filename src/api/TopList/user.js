import { request } from '../../utils/request.js'

export const getUserLogin = (data) => request({
  /*
  @params userName 用户名 String
  @params password 密码 String

  @return  userInfo 用户信息
  */
  method: 'POST',
  url: '/v1_0/authorizations',
  data
})

export const sendSmsCode = (mobile) => request({
  /*
  获取验证码
  @params mobile

  */
  method: 'GET',
  url: `/v1_0/sms/codes/${mobile}`
})

export const getUserInfor = (mobile) => request({
  /*
  获取用户信息
  @params mobile

  */
  method: 'GET',
  url: '/v1_0/user'
  // headers: {
  //   Authorization: `Bearer ${store.state.user.token}`
  // }
})

export const getUserChannels = (mobile) => request({
  /*
  获取用户频道列表
  @params mobile

  */
  method: 'GET',
  url: '/v1_0/user/channels'
  // headers: {
  //   Authorization: `Bearer ${store.state.user.token}`
  // }
})
export const addCollect = (data) => request({
  /*

    @return   收藏文章
    */
  method: 'POST',
  url: '/v1_0/article/collections',
  data
})
export const delCollect = (target) => request({
  /*

    @return   取消收藏文章
    @params target 目标文章
    */
  method: 'DELETE',
  url: `/v1_0/article/collections/${target}`
})
export const addLike = (data) => request({
  /*

    @return   喜欢文章
    */
  method: 'POST',
  url: '/v1_0/article/likings',
  data
})
export const delLike = (target) => request({
  /*

    @return   取消喜欢文章
    @params target 目标文章
    */
  method: 'DELETE',
  url: `/v1_0/article/likings/${target}`
})
