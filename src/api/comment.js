import { request } from '../utils/request.js'
export const getComments = params => request({
  /*

      @return   关注用户
      @params target 目标用户
      */
  method: 'GET',
  url: '/v1_0/comments',
  params
})
export const addComment = data => request({
  /*

      @return

      */
  method: 'POST',
  url: '/v1_0/comments',
  data
})
export const deleteCommentLike = target => request({
  /*

      @return   取消点赞
      @params target 目标评论id
      */
  method: 'DELETE',
  url: `/v1_0/comment/likings/${target}`
})
export const addCommentLike = data => request({
  /*

      @return   取消点赞
      @params target 目标评论id
      */
  method: 'POST',
  url: '/v1_0/comment/likings/',
  data
})
