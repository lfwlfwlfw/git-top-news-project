import { getUserLogin, sendSmsCode, getUserInfor, getUserChannels, addCollect, delCollect, addLike, delLike } from './TopList/user.js'

import { getArticleList, getArticleDetail, addFollows, delFollows } from './ContentList/artilcle.js'

import { getAllChannels, addUserChannels, deleteUserChannels } from './allchannels.js'

import { getComments, addComment, deleteCommentLike, addCommentLike } from './comment.js'

import { getSuggestions, getResults, getSearchHistories } from './search.js'
export const getUserLoginAPI = getUserLogin
export const sendSmsCodeAPI = sendSmsCode
export const getUserInforAPI = getUserInfor
export const getUserChannelsAPI = getUserChannels
export const addCollectAPI = addCollect
export const delCollectAPI = delCollect
export const delLikeAPI = delLike
export const addLikeAPI = addLike
// artilcle
export const getArticleListAPI = getArticleList
export const getArticleDetailAPI = getArticleDetail
export const addFollowsAPI = addFollows
export const delFollowsAPI = delFollows

// channels
export const getAllChannelsAPI = getAllChannels
export const addUserChannelsAPI = addUserChannels
export const deleteUserChannelsAPI = deleteUserChannels
// Suggestions
export const getSuggestionsAPI = getSuggestions
export const getResultsAPI = getResults
export const getSearchHistoriesAPI = getSearchHistories
// comment
export const getCommentsAPI = getComments
export const addCommentAPI = addComment
export const deleteCommentLikeAPI = deleteCommentLike
export const addCommentLikeAPI = addCommentLike
