import { request } from '../../utils/request.js'

export const getArticleList = (params) => request({
  /*

    @return   获取文章列表
    */
  method: 'GET',
  url: '/v1_0/articles',
  params
})
export const getArticleDetail = (articleId) => request({
  /*

    @return   获取新闻详情
    @params articleId 文章ID
    */
  method: 'GET',
  url: `/v1_0/articles/${articleId}`
})
export const addFollows = (data) => request({
  /*

    @return   关注用户
    @params articleId 文章ID
    */
  method: 'POST',
  url: '/v1_0/user/followings',
  data
})
export const delFollows = (target) => request({
  /*

    @return   关注用户
    @params target 目标用户
    */
  method: 'DELETE',
  url: `/v1_0/user/followings/${target}`
})
