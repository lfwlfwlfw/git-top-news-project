import Vue from 'vue'
import VueRouter from 'vue-router'
// import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/layout',
    redirect: '/layout/home',
    component: () => import(/* webpackChunkName: "about" */ '../views/layout/layout.vue'),
    children: [
      {
        path: 'home',
        component: () => import(/* webpackChunkName: "about" */ '../views/home/home.vue')
      },
      {
        path: 'qa',
        component: () => import(/* webpackChunkName: "about" */ '../views/qa/qa.vue')
      },
      {
        path: 'video',
        component: () => import(/* webpackChunkName: "about" */ '../views/video/video.vue')
      },
      {
        path: 'my',
        component: () => import(/* webpackChunkName: "about" */ '../views/my/my.vue')
      }

    ]
  },
  {
    path: '/login',
    component: () => import(/* webpackChunkName: "about" */ '../views/login.vue')
  },
  {
    path: '/search',
    component: () => import(/* webpackChunkName: "about" */ '../views/search/search.vue')
  },
  {
    path: '/article/:articleId',
    component: () => import(/* webpackChunkName: "about" */ '../views/article/article.vue')
  }
  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView
  // },
  // {
  //   path: '/about',
  //   name: 'about'
  // route level code-splitting
  // this generates a separate chunk (about.[hash].js) for this route
  // which is lazy-loaded when the route is visited.
  // component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
  // {
  //   path: '/',
  //   redirect: {
  //     path: '/login'
  //   }
  // }
]

const router = new VueRouter({
  routes
})

export default router
